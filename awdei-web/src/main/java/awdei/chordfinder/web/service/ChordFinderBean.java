package awdei.chordfinder.web.service;

import awdei.chordfinder.dto.RequestDTO;
import awdei.chordfinder.dto.ResponseDTO;
import awdei.chordfinder.web.bean.BaseBean;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.apache.log4j.Logger;
import org.mule.api.MuleContext;
import org.mule.api.MuleException;
import org.mule.api.MuleMessage;
import org.mule.api.context.MuleContextFactory;
import org.mule.context.DefaultMuleContextFactory;
import org.mule.module.client.MuleClient;

/**
 *
 * @author Michael Sobotka
 */
@ManagedBean(name = "chordFinderBean")
@ViewScoped
public class ChordFinderBean extends BaseBean {

    private static Logger log = Logger.getLogger(ChordFinderBean.class);
    private static final String CHORDFINDER_URL = "tcp://localhost:14001";
    private static final String MESSAGEBOX_REQUEST = "requestForm";
    private MuleContextFactory muleContextFactory;
    private MuleContext muleContext;
    private MuleClient muleClient;
    private RequestDTO requestDTO = new RequestDTO();
    private ResponseDTO responseDTO;
    private boolean noResult = false;

    public ChordFinderBean() {
    }

    @PostConstruct
    public void init() {
        try {

            muleContextFactory = new DefaultMuleContextFactory();
            muleContext = muleContextFactory.createMuleContext();
            muleContext.start();
            muleClient = new MuleClient(muleContext);
        } catch (Exception e) {
            log.error("Could not connect to Mule.", e);
        }
    }

    @PreDestroy
    public void preDestroy() {
        try {
            muleContext.stop();
        } catch (MuleException e) {
            log.error("Error on @PreDestroy.", e);
        }
    }

    public void initiateRequest() {
        log.info("User initiated request: " + requestDTO);

        // Send message to Mule
        try {
            MuleMessage result = muleClient.send(CHORDFINDER_URL, requestDTO, null, 2 * 60 * 1000);
            log.info("Successfully sent request to Mule.");
            if (result.getPayloadAsBytes() == null) {
                log.info("Payload was null.");
                noResult = true;
            } else {
                log.info("Extracting payload.");
                responseDTO = deserialize(result.getPayloadAsBytes());
            }
        } catch (Exception e) {
            log.error("Could not connect to Mule.", e);
        } finally {
            // clean up
            requestDTO = new RequestDTO();
        }
    }

    private ResponseDTO deserialize(byte[] data) throws ClassNotFoundException, IOException {
        ByteArrayInputStream in = new ByteArrayInputStream(data);
        ObjectInputStream is = new ObjectInputStream(in);
        ResponseDTO response = (ResponseDTO) is.readObject();
        is.close();

        return response;
    }

    public RequestDTO getRequestDTO() {
        return requestDTO;
    }

    public void setRequestDTO(RequestDTO requestDTO) {
        this.requestDTO = requestDTO;
    }

    public ResponseDTO getResponseDTO() {
        return responseDTO;
    }

    public boolean isNoResult() {
        return noResult;
    }
}
