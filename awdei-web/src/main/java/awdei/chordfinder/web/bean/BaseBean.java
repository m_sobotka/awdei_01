package awdei.chordfinder.web.bean;

import java.io.IOException;
import java.io.Serializable;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.NoneScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;

/**
 * This class provides convenience-methods that are useful in other
 * managed-beans. Extend other managed-beans from this bean. Use the annotations
 * "@ManagedBean" and the scope you like, e.g. "@RequestScoped".
 *
 * @author Michael Sobotka
 *
 */
@ManagedBean
@NoneScoped
public abstract class BaseBean implements Serializable {

    private static final Logger log = Logger.getLogger(BaseBean.class);

    /**
     * Redirects the user to a given page with respect to the contextpath.
     *
     * @param toPage The page you want the user to redirect to without slashes.
     * E.g.: index.xthml and NOT /index.xhtml or similar.
     */
    protected void redirect(String toPage) {
        String contextPath = "";
        try {
            ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
            contextPath = context.getRequestContextPath();
            context.redirect(contextPath + "/" + toPage);
        } catch (IOException e) {
            log.error("Could not redirect user to " + contextPath + "/" + toPage, e);
        }
        log.debug("User got redirected to: " + toPage);
    }

    /**
     * Prints a message on the screen. Use it if you want to inform the user.
     *
     * @param message The output message.
     * @param severity The severity of the message, e.g.
     * FacesMessage.SEVERITY_INFO
     * @param messageBoxId The id of the message box you want to display the
     * message in. If null, the message gets printed to the first message-box
     * available (fine in most cases).
     */
    protected void printMessageOnScreen(String message, FacesMessage.Severity severity, String messageBoxId) {
        FacesMessage facesMessage = new FacesMessage(severity, message, "");
        FacesContext.getCurrentInstance().addMessage(messageBoxId, facesMessage);
    }

    /**
     * Retrieves a given HTTP-GET-Parameter by its name. (e.g.
     * url.xhtml?search=searchString -> returns searchString)
     *
     * @param parameterName The name of the desired parameter.
     * @return The value of the GET-Parameter or null if the parameter is not
     * there.
     */
    protected String retrieveGetParameter(String parameterName) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return facesContext.getExternalContext().getRequestParameterMap().get(parameterName);
    }

    /**
     * Writes an object into the Session Map.
     *
     * @param propertyName The name of the property.
     * @param propertyValue The value of the property.
     */
    protected void setSessionProperty(String propertyName, Object propertyValue) {
        Map<String, Object> sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
        sessionMap.put(propertyName, propertyValue);
    }

    /**
     * Retrieves an object from the Session Map by its name.
     *
     * @param propertyName The name of the property.
     * @return The object that should be retrieved according to its name (can be
     * null if not found).
     */
    protected Object getSessionProperty(String propertyName) {
        Map<String, Object> sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
        return sessionMap.get(propertyName);
    }
}
