package awdei.chordfinder.dto;

import java.io.Serializable;

/**
 * Represents a chordfinder-request performed by the user.
 *
 * @author Michael Sobotka
 *
 */
public class RequestDTO implements Serializable {

    private String artist;
    private String song;

    public RequestDTO() {
    }

    public RequestDTO(String artist, String song) {
        this.artist = artist;
        this.song = song;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getSong() {
        return song;
    }

    public void setSong(String song) {
        this.song = song;
    }

    @Override
    public String toString() {
        return "RequestDTO{" + "artist=" + artist + ", song=" + song + '}';
    }
}
