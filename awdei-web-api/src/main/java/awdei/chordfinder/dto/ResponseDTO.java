package awdei.chordfinder.dto;

import java.io.Serializable;

import awdei.chordfinder.entity.Song;

/**
 * Represents a chordfinder-response containing the computed results.
 * 
 * @author Michael Sobotka
 * 
 */
public class ResponseDTO implements Serializable {
	
	private static final long serialVersionUID = -5681756549874523850L;
	
	private Song song;

	public ResponseDTO() {
	}

	public ResponseDTO(Song song) {
		this.song = song;
	}

	public Song getSong() {
		return song;
	}

	public void setSong(Song song) {
		this.song = song;
	}

}
