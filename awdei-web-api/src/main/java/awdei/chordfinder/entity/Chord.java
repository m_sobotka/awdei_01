package awdei.chordfinder.entity;

import java.io.Serializable;

public class Chord implements Serializable {

	private static final long serialVersionUID = -7338253197766534897L;
	
	private String name;
	private String imageURL;

	public Chord() {
	}

	public Chord(String name, String imageURL) {
		this.name = name;
		this.imageURL = imageURL;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImageURL() {
		return imageURL;
	}

	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}

	@Override
	public String toString() {
		return "[" + name + "]";
	}

}
