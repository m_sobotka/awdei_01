package awdei.chordfinder.entity;

import java.io.Serializable;
import java.util.List;

public class Tab implements Serializable {

	private static final long serialVersionUID = 8564020333469556199L;
	
	private String url;
	private String description;
	private String type;
	private int rating;
	private List<Chord> chords;

	public Tab() {
	}

	public Tab(String url, String description, String type, int rating, List<Chord> chords) {
		this.url = url;
		this.description = description;
		this.type = type;
		this.rating = rating;
		this.chords = chords;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public List<Chord> getChords() {
		return chords;
	}

	public void setChords(List<Chord> chords) {
		this.chords = chords;
	}

}
