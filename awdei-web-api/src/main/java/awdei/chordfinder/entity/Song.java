package awdei.chordfinder.entity;

import java.io.Serializable;
import java.util.List;

public class Song implements Serializable {

	private static final long serialVersionUID = -8343758735788015493L;
	
	private String title;
	private String artist;
	private List<Tab> tabs;

	public Song() {
	}

	public Song(String title, String artist, List<Tab> tabs) {
		this.title = title;
		this.artist = artist;
		this.tabs = tabs;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

	public List<Tab> getTabs() {
		return tabs;
	}

	public void setTabs(List<Tab> tabs) {
		this.tabs = tabs;
	}

}
