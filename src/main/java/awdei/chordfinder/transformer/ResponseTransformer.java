package awdei.chordfinder.transformer;
import java.util.List;

import org.apache.log4j.Logger;
import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.transformer.AbstractMessageTransformer;
import org.mule.transformer.types.DataTypeFactory;

import awdei.chordfinder.dto.ResponseDTO;
import awdei.chordfinder.entity.Song;

public class ResponseTransformer extends AbstractMessageTransformer {
	
	private static final Logger log = Logger.getLogger(ResponseTransformer.class);
	
	public ResponseTransformer() {
		super();
		registerSourceType(DataTypeFactory.OBJECT);
		setReturnDataType(DataTypeFactory.create(ResponseDTO.class));
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {
		
		log.info("Aggregating response of type: " + message.getPayload().getClass());
		
		ResponseDTO result = new ResponseDTO();
		
		List<Object> aggregatedScraperResults = (List<Object>) message.getPayload();
		
		for(Object r: aggregatedScraperResults) {
			log.info("Received result of type: " + r.getClass());
			if(r instanceof Song) {
				result.setSong((Song) r);
			} else {
				// extendable to other return types!
			}
		}

		return result;
	}
}
