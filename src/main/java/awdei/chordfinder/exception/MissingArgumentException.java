package awdei.chordfinder.exception;

public class MissingArgumentException extends Exception {

	private static final long serialVersionUID = -1807033555111770110L;

	public MissingArgumentException(String message) {
		super(message);
	}

}
