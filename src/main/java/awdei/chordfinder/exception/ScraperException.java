package awdei.chordfinder.exception;

public class ScraperException extends Exception {

	private static final long serialVersionUID = -5242392294108900786L;

	public ScraperException(String message) {
		super(message);
	}

}
