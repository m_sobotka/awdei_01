package awdei.chordfinder.scraper;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import awdei.chordfinder.entity.Chord;
import awdei.chordfinder.entity.Song;
import awdei.chordfinder.entity.Tab;

public class GuitarChords247 {
	
	private static final Logger log = Logger.getLogger(GuitarChords247.class);

	/**
	 * No parameters, just append the chord's name, like C#, Em, Asus4, etc.
	 */
	private static final String ROOT_URL = "http://www.guitarchords247.com/roots/?browseset=";
	
	private List<Chord> chordCache = new ArrayList<Chord>();
	
	public Song scrape(Song song) {
		
		log.info("Searching for chords from www.guitarchords247.com");
		
		for (Tab tab : song.getTabs()) {
			for (Chord chord : tab.getChords()) {
				
				// Lookup a given chord
				String chordName = "";
				String finalURL = "";
				Document doc;
				try {
					chordName = URLEncoder.encode(chord.getName(), "UTF-8");
					finalURL = ROOT_URL + chordName;
					doc = Jsoup.connect(finalURL).get();
				} catch (Exception e) {
					log.error("Chord lookup failed for: " + finalURL, e);
					continue;
				}
				
				// Extract the imageURL for this chord
				Elements chords = doc.getElementsByClass("ctitle");
				
				for (Element c : chords) {
					String cName = c.text().replace(" Chord", "");
					if (chordName.toLowerCase().equals(cName.toLowerCase())) {
						log.info("Chord matches: " + cName);
						
						// first lookup in cache
						Chord cachedChord = getCachedChord(chordName);
						if (cachedChord != null) {
							chord.setImageURL(cachedChord.getImageURL());
							break;
						}
						
						// if not in cache, fetch imageURL
						try {
							Element img_image = c.nextElementSibling().nextElementSibling();
							String imageURL = img_image.attr("src");
							chordCache.add(new Chord(chordName, imageURL));
							chord.setImageURL(imageURL);
							log.info("Found chord: " + cName + " | " + imageURL);
						} catch (Exception e) {
							// if some error occurred, just continue with the next result
							log.error(e.getMessage());
							continue;
						}
						break;
					}
				}
			}
		}
		
		// imageURLs were updated (in case of success)
		return song;
	}
	
	private Chord getCachedChord(String chordName) {
		for (Chord c : chordCache) {
			if (chordName.toLowerCase().equals(c.getName().toLowerCase())) {
				return c;
			}
		}
		return null;
	}

}
