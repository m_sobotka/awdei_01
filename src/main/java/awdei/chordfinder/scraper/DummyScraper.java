package awdei.chordfinder.scraper;

import awdei.chordfinder.dto.RequestDTO;
import awdei.chordfinder.exception.MissingArgumentException;
import awdei.chordfinder.exception.ScraperException;

public class DummyScraper {
	
	public String scrape(RequestDTO request) throws ScraperException, MissingArgumentException {
		// Not implemented, only for illustrating purposes.
		return "Response from DummyScraper";
	}

}
