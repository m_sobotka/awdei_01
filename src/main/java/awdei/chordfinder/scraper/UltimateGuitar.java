package awdei.chordfinder.scraper;

import java.io.IOException;
import java.net.URLEncoder;

import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import awdei.chordfinder.dto.RequestDTO;
import awdei.chordfinder.entity.Chord;
import awdei.chordfinder.entity.Song;
import awdei.chordfinder.entity.Tab;
import awdei.chordfinder.exception.MissingArgumentException;
import awdei.chordfinder.exception.ScraperException;

import java.util.ArrayList;
import java.util.List;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class UltimateGuitar {

	private static final Logger log = Logger.getLogger(UltimateGuitar.class);

	/**
	 * GET parameters are: 
	 * - band_name 
	 * - song_name
	 */
	private static final String ROOT_URL = "http://www.ultimate-guitar.com/search.php?type[]=300&type2[]=40000";

	public Song scrape(RequestDTO request) throws ScraperException, MissingArgumentException {
		
		log.info("Searching for tabs from www.ultimate-guitar.com");
		
		Song result = new Song();
		result.setArtist(request.getArtist());
		result.setTitle(request.getSong());
		
		Document doc;
		
		String finalURL = ROOT_URL;
		String artist = request.getArtist();
		String song = request.getSong();
		
		// Validate Input
		if ((artist == null || artist.isEmpty()) && (song == null || song.isEmpty())) {
			throw new MissingArgumentException("You must provide at least an artist or song.");
		}

		// Connect to URL
		try {
			artist = URLEncoder.encode(artist, "UTF-8");
			song = URLEncoder.encode(song, "UTF-8");
			
			// add GET-parameters to the search query
			if (artist != null && !artist.isEmpty()) {
				finalURL += "&band_name=" + artist;
			}
			if (song != null && !song.isEmpty()) {
				finalURL += "&song_name=" + song;
			}
			
			doc = Jsoup.connect(finalURL).get();
			log.info("Successfully connected to: " + finalURL);
		} catch (IOException e) {
			log.error("Could not connect to url: " + finalURL, e);
			throw new ScraperException("Could not scrape url: " + finalURL + ". Reason: " + e.getMessage());
		}
		
		Element songTable = null;
		try {
			songTable = doc.getElementsByClass("tresults").first();
		} catch (NullPointerException e) {
			log.info("No results found...");
			throw new ScraperException("No results found for song: " + song);
		}
		
		List<Tab> tabs = new ArrayList<Tab>();
		
        Elements rows = songTable.select("tr");

        // iterate over all rows
		for (Element currentRow : rows) {		
			try {
				// tab type
				Element strong_tabType = currentRow.select("td:eq(3)").first();
				String tabType = strong_tabType.text();
				
				// process only tab types = "chords"
				if (!tabType.equals("chords")) {
					continue;
				}
				
				// tab link
				Element a_song = currentRow.getElementsByClass("song").first();
				String tabName = a_song.text();
				String tabLink = a_song.attr("href");
				
				// rating
				Element span_rating = currentRow.getElementsByClass("rating").first();
				int rating = 0;
				if (span_rating != null) {
					String ratingC = span_rating.child(0).className();
					if ("r_1".equals(ratingC)) {
						rating = 1;
					} else if ("r_2".equals(ratingC)) {
						rating = 2;
					} else if ("r_3".equals(ratingC)) {
						rating = 3;
					} else if ("r_4".equals(ratingC)) {
						rating = 4;
					} else if ("r_5".equals(ratingC)) {
						rating = 5;
					}
				}
				
				// parse tabs for chords
				List<Chord> chords = processDetailPage(tabLink);
				
				// add the current tab including its chords to the result
				tabs.add(new Tab(tabLink, tabName, tabType, rating, chords));
				
	        	log.info("Successfully processed tab: " + tabName + " | " + tabType + " | " + rating + " " + " | " + tabLink + " | " + chords);
			} catch(Exception e) {
				// If some row caused some error, just leave it...
				log.error(e);
				continue;
			}
        }
		
		// add the processed tabs to the final result
		result.setTabs(tabs);
		
		return result;
	}
	
	private List<Chord> processDetailPage(String url) throws IOException  {
		log.info("Processing Detail page: " + url);
		
		Document tabPage = Jsoup.connect(url).get();
		Element tab = tabPage.select("#cont pre").get(1);
		
		List<String> chords = new ArrayList<String>();
		
		// span's contain chord names
		Elements span_chords = tab.getElementsByTag("span");
		
		for (Element c : span_chords) {
			String chord = c.text();
			if (!chords.contains(chord)) {
				chords.add(chord);
			}
		}
		
		// convert chord-strings to Chord objects
		List<Chord> result = new ArrayList<Chord>(chords.size());
		for (String c : chords) {
			result.add(new Chord(c, null));
		}
		
		return result;
	}
}
